package ru.andreymarkelov.atlas.plugins.templator;

public class Counter {
    private static long curr = System.currentTimeMillis();

    public static synchronized long getValue() {
        return curr++;
    }

    private Counter() {}
}
