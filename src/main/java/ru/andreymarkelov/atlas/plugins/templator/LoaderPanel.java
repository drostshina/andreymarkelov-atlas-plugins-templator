package ru.andreymarkelov.atlas.plugins.templator;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.JiraWebUtils;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;

@SuppressWarnings("unchecked")
public class LoaderPanel extends AbstractJiraContextProvider {
    private final TemplateStoreDataService dataService;
    private final VelocityRequestContextFactory velocityRequestContextFactory;

    public LoaderPanel(TemplateStoreDataService dataService, VelocityRequestContextFactory velocityRequestContextFactory) {
        this.dataService = dataService;
        this.velocityRequestContextFactory = velocityRequestContextFactory;
    }

    @Override
    public Map<String, Object> getContextMap(ApplicationUser user, JiraHelper helper) {
        Boolean inStatus = Boolean.FALSE;
        Issue issue = (Issue) helper.getContextParams().get("issue");
        for (TemplateStoreData data : dataService.getTemplateStoreDataList()) {
            if (data != null
                   && data.getStatuses().contains(issue.getStatusId())
                   && data.getProjectKey().equals(issue.getProjectObject().getKey())
                   && data.getIssueTypeKey().equals(issue.getIssueTypeId())) {
                inStatus = Boolean.TRUE;
            }
        }

        Map<String, Object> contextMap = new HashMap<String, Object>();
        contextMap.put("basePath", velocityRequestContextFactory.getJiraVelocityRequestContext().getBaseUrl());
        contextMap.put("issue", issue);
        contextMap.put("inStatus", inStatus);
        return contextMap;
    }
}
