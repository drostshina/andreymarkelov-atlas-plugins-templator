package ru.andreymarkelov.atlas.plugins.templator;

import java.util.List;

public interface TemplateStoreDataService {
    TemplateStoreData getTemplateStoreData(Long id);
    List<TemplateStoreData> getTemplateStoreDataList();
    void setTemplateStoreData(Long id, TemplateStoreData data);
}
