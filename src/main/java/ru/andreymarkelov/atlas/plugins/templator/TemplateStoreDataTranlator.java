package ru.andreymarkelov.atlas.plugins.templator;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;

public class TemplateStoreDataTranlator {
    public static TemplateStoreData fromString(String jsonStr) {
        if (jsonStr == null || jsonStr.length() == 0) {
            return null;
        }

        try {
            JSONObject json = new JSONObject(jsonStr);
            TemplateStoreData data = new TemplateStoreData();
            if (json.has("fileName")) {
                data.setFileName(json.getString("fileName"));
            }
            if (json.has("templateName")) {
                data.setTemplateName(json.getString("templateName"));
            }
            if (json.has("statuses")) {
                data.setStatuses(strToList(json.getString("statuses")));
            }
            if (json.has("updateTime")) {
                data.setUpdateTime(json.getLong("updateTime"));
            }
            if (json.has("id")) {
                data.setId(json.getLong("id"));
            }
            if (json.has("projectKey")) {
                data.setProjectKey(json.getString("projectKey"));
            }
            if (json.has("issueTypeKey")) {
                data.setIssueTypeKey(json.getString("issueTypeKey"));
            }
            return data;
        } catch (JSONException e) {
            return null;
        }
    }

    private static String listToStr(List<String> list) {
        if (list == null) {
            return "";
        }

        JSONArray jsonArray = new JSONArray();
        for (String s : list) {
            jsonArray.put(s);
        }
        return jsonArray.toString();
    }

    private static List<String> strToList(String str) {
        List<String> list = new ArrayList<String>();
        try {
            JSONArray jsonArray = new JSONArray(str);
            for (int i = 0; i < jsonArray.length(); i++) {
                list.add(jsonArray.getString(i));
            }
            return list;
        } catch (JSONException e) {
            return list;
        }
    }

    public static String toString(TemplateStoreData data) {
        if (data == null) {
            return null;
        }

        JSONObject json = new JSONObject();
        try {
            json.put("fileName", data.getFileName());
            json.put("templateName", data.getTemplateName());
            json.put("statuses", listToStr(data.getStatuses()));
            json.put("updateTime", data.getUpdateTime());
            json.put("id", data.getId());
            json.put("projectKey", data.getProjectKey());
            json.put("issueTypeKey", data.getIssueTypeKey());
        } catch (JSONException e) {
            return null;
        }
        return json.toString();
    }

    private TemplateStoreDataTranlator() {
    }
}
